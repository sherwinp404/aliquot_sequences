import math
import time

def sum_of_divisors(n):

	if n == 0:
		return 0

	if n == 1:
		return 0

	sum = 1

	for i in range (2, n):
		if n%i == 0:
			sum = sum+i
	return sum


def aliquot_calculator(n, t):
	sequence=[n]
	for i in range(t):
		next_term = sum_of_divisors(n)
		if next_term > 10**9:
			break
		if next_term == 0:
			sequence.append(next_term)
			#print("Zero reached")
			break
		sequence.append(next_term)
		n = next_term
	#print(sequence)
	return sequence


def loop_checker(sequence):
	value_set = set(sequence)
	if len(value_set)< len(sequence): 
		return True
	return False


def sequence_classifier(sequence, t):
	if sequence[-1] == 0:
		#print("Terminates")
		return 0
	elif loop_checker(sequence):
		#print("Loops")
		return 1
	elif len(sequence)<t+1:
		#print("Ceiling reached")
		return 2
	elif len(sequence)==t+1:
		#print("idk")
		return 3
	return 5

def number_tester(k, t):
	classification = [[],[],[],[]]
	for i in range(k):
		type = sequence_classifier(aliquot_calculator(i,t), t)
		if type == 5:
			print("BROKEN")
			print(i)
			break
		classification[type].append(i)
	for i in range(4):
		print(classification[i])
		print("---------------------------------------------------------------------------")

"""t0 = time.time()
number_tester(500, 30)
t1 = time.time()"""

"""print("Execution time = "+ str(t1-t0))"""


# 200 terms took 3.588
# 500 terms took 24.23

def sum_of_proper_divisors_prime(p, k):
	return ((p**(k))-1)/(p-1)

def sum_of_divisors_prime(p, k):
	return ((p**(k+1))-1)/(p-1) 


####################################################


def smallest_factor(n):
    """Returns the smallest factor of a positive integer n."""
    sqrt=n**0.5
    i=2
    while i<=sqrt:
        if n%i==0:
            return i                            #If we get here, return i as the value.
        i+=1
    return n  

def decompose(n):
    """Generates a dictionary representing the prime decomposition."""
    factors={}
    current_number=n                            #divide current_number by the factor found found until it reaches 1
    while current_number > 1:
        p=smallest_factor(current_number)
        if p in factors.keys():                 #if p is not a new factor, increase the power
            factors[p]+=1
        else:
            factors[p]=1                        #if p is a new factor, create a new entry
        current_number = current_number//p
    return factors

def make_mult_func(fun_pp):
    """Takes a function fun_pp that is only defined on prime power and returns the associated
    multiplicative function defined on all positive integers."""
    def mult_version(n):                        #The output should be another function -- this one
        n=decompose(n)
        #print("decomposition: " +str(n))
        result=1
        for p in n.keys():
            result*=fun_pp(p,n[p])              #Uses the value at a prime power.
        return result
    return mult_version                         #Return the function made.


 ######################################

def sum_of_divisors_fast(n):
	if n == 0:
		return 0
	sum = make_mult_func(sum_of_divisors_prime)(n) - n
	return sum

def divisor_checker(n):
	for i in range(1,n):
		if sum_of_divisors(i) != sum_of_divisors_fast(i):
			print("BROKEN = " + str(i))
	print("FIN")


"""t0 = time.time()
number_tester(500, 30)
t1 = time.time()"""

"""print("Execution time = "+ str(t1-t0))"""

"""print("Execution time = "+ str(t1-t0))"""


# 200 terms took 3.588
# 500 terms took 24.23



def aliquot_calculator_fast(n, t):
	sequence=[n]
	for i in range(t):
		next_term = sum_of_divisors_fast(n)
		if next_term > 10**9:
			break
		if next_term == 0:
			sequence.append(next_term)
			#print("Zero reached")
			break
		sequence.append(next_term)
		n = next_term
	#print(sequence)
	return sequence


def number_tester_fast(k, t):
	classification = [[],[],[],[]]
	for i in range(k):
		type = sequence_classifier(aliquot_calculator_fast(i,t), t)
		if type == 5:
			print("BROKEN")
			print(i)
			break
		classification[type].append(i)
	for i in range(4):
		print(classification[i])
		print("---------------------------------------------------------------------------")


t0 = time.time()
slow=number_tester(500,30)
fast=number_tester_fast(500, 30)
print("-----------------------------------")
print(slow==fast)
t1 = time.time()

print("Execution time = "+ str(t1-t0))

#500 terms took 0.054s
#2000 terms took 0.367s
#10000 terms took 3.314s
#20000 terms took 9.854s